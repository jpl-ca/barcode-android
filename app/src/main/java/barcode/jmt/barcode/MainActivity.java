package barcode.jmt.barcode;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import barcode.jmt.barcode.fragment.BarCodeCreateFragment;
import barcode.jmt.barcode.fragment.BarCodeReadFragment;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {
    private ViewPager mViewPager;
    private ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.addTab(actionBar.newTab().setText(getString(R.string.s_leer)).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(getString(R.string.s_generar)).setTabListener(this));
        mViewPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager()));
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {actionBar.setSelectedNavigationItem(position);}
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
            @Override
            public void onPageScrollStateChanged(int arg0) {}
        });
    }
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        int pos = tab.getPosition();
        mViewPager.setCurrentItem(pos);
    }
    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }
    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }
    public class TabsPagerAdapter extends FragmentPagerAdapter{
        public TabsPagerAdapter(FragmentManager fm){
            super(fm);
        }
        @Override
        public Fragment getItem(int index) {
            Fragment fragment = null;
            Bundle args = new Bundle();switch (index) {
                case 0: fragment = new BarCodeReadFragment();break;
                case 1: fragment = new BarCodeCreateFragment();break;
            }
            fragment.setArguments(args);
            return fragment;
        }
        @Override
        public int getCount() {
            return 2;
        }
    }
}