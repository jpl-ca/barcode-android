package barcode.jmt.barcode.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import barcode.jmt.barcode.R;
public class BarCodeCreateFragment extends Fragment{
    ImageView iv;
    EditText txt;
    Button btn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_barcode_create, container, false);
        iv = ((ImageView) rootView.findViewById(R.id.iv));
        txt = ((EditText) rootView.findViewById(R.id.txt));
        btn = ((Button) rootView.findViewById(R.id.btn));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createQR(txt.getText().toString());
            }
        });
        return rootView;
    }

    private void createQR(String data) {
        com.google.zxing. MultiFormatWriter writer =new MultiFormatWriter();
        String finaldata = Uri.encode(data, "utf-8");
        BitMatrix bm = null;
        try {
            bm = writer.encode(finaldata, BarcodeFormat.QR_CODE,150, 150);
            Bitmap ImageBitmap = Bitmap.createBitmap(180, 145, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < 180; i++) {//width
                for (int j = 0; j < 145; j++) {//height
                    ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
            if (ImageBitmap != null) {
                iv.setImageBitmap(ImageBitmap);
            } else {
                Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
}