Welcome to BarCodeJM!
===================
Resumen acerca de leer y crear codigo de barras con una aplicacion.
----------

El diagrama de flujo es:
![flowBC.png](https://bitbucket.org/repo/p5Mdr7/images/1237268473-flowBC.png)

**VIEWPAGER**
Para mostrar las 2 opciones del Lector de codigo de barras se implemento un ViewPager.

ViewPager: Contiene los fragments y requiere de PagerTitleStrip para mostrar los Tabs
```
#!xml

<?xml version="1.0" encoding="utf-8"?>
<android.support.v4.view.ViewPager
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/pager"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <android.support.v4.view.PagerTitleStrip
        android:id="@+id/pager_title_strip"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_gravity="top"
        android:background="#33b5e5"
        android:textColor="#fff"
        android:paddingTop="4dp"
        android:paddingBottom="4dp"/>
</android.support.v4.view.ViewPager>
```

ViewPager necesita un FragmentPagerAdapter para Controlar los Fragments que van a contener cuando navega de un Tab hacia otro.
FragmentPagerAdapter por medio del metodo getItem retorna uno de los Fragments segun el indice recibido.
Se ha creado 2 Fragments(BarCodeReadFragment y BarCodeCreateFragment) para escanear y generar un codigo de barras respectivamente.


**BARCODE READER**
Para trabajar con el lector de código de barras se requiere de una librería, en este caso es "zxing", el cual por medio de la camara del celular lee el codigo y devuelve su valor o genera codigos de barras a partir de un texto dado.

Para esta demo se creo 2 Fragments ya mencionadas, que se describen a continuacion.
BarCodeReadFragment: Se asigna la vista ZXingScannerView para el lector.

```
#!java

private ZXingScannerView mScannerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mScannerView = new ZXingScannerView(getActivity());
        return mScannerView;
    }
```

BarCodeCreateFragment: Apartir de la matriz generada por la libreria se crea un BitMap el cual sera mostrado en un ImageView.

```
#!java

private void createQR(String data) {
        com.google.zxing. MultiFormatWriter writer =new MultiFormatWriter();
        String finaldata = Uri.encode(data, "utf-8");
        BitMatrix bm = null;
        try {
            bm = writer.encode(finaldata, BarcodeFormat.QR_CODE,150, 150);
            Bitmap ImageBitmap = Bitmap.createBitmap(180, 145, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < 180; i++) {//width
                for (int j = 0; j < 145; j++) {//height
                    ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
            if (ImageBitmap != null) {
                iv.setImageBitmap(ImageBitmap);
            } else {
                Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
```

![Screenshot_2015-05-13-11-13-14.png](https://bitbucket.org/repo/p5Mdr7/images/3561359267-Screenshot_2015-05-13-11-13-14.png)